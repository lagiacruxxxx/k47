<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['menupoint_home'] = "home";
$lang['menupoint_dinner'] = "formal diner";
$lang['menupoint_lounge'] = "lounge";
$lang['menupoint_bar'] = "bar";
$lang['menupoint_rooftop'] = "roof garden";
$lang['menupoint_christmas'] = "christmas party";
$lang['menupoint_pressconference'] = "press conference";


$lang['home_content_text'] = "";

$lang['pressconference_content_header'] = "press conference";
$lang['pressconference_content'] = "";

$lang['lounge_content_header'] = "lounge";
$lang['lounge_content'] = "";

$lang['bar_content_header'] = "bar";
$lang['bar_content'] = "";

$lang['dinner_content_header'] = "formal diner";
$lang['dinner_content'] = "";

$lang['rooftop_content_header'] = "roof garden";
$lang['rooftop_content'] = "";

$lang['christmas_content_header'] = "christmas party";
$lang['christmas_content'] = "";

$lang['home_content_menu_download'] = "Factsheet";
$lang['home_content_menu_pictures'] = "Pictures";
$lang['home_content_menu_press'] = "Press";
$lang['home_content_menu_contact'] = "Contact";
$lang['home_content_menu_impress'] = "Impress";
$lang['home_content_menu_campaign'] = "Campaign";

$lang['footer_text'] = "k47.wien • Franz-Josefs-Kai 47 • 1010 Wien • +43 1 22 600 28 • <a href='mailto:office@k47.wien'>office@k47.wien</a>";


$lang['location_factsheet'] = "k47.wien location presentation";
$lang['location_filename'] = "k47.wien_presentation_location_en.pdf";

$lang['location_factsheet2'] = "k47.wien EM Lounge";
$lang['location_filename2'] = "k47.wien_EM_Lounge.pdf";