<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['menupoint_home'] = "home";
$lang['menupoint_dinner'] = "galadinner";
$lang['menupoint_lounge'] = "lounge";
$lang['menupoint_bar'] = "bar";
$lang['menupoint_rooftop'] = "dachgarten";
$lang['menupoint_christmas'] = "weihnachtsfeier";
$lang['menupoint_pressconference'] = "pressekonferenz";


$lang['home_content_text'] = "Eine wunderbare Heiterkeit hat meine ganze Seele eingenommen, gleich den süßen Frühlingsmorgen, die ich mit ganzem Herzen genieße. Ich bin allein und freue mich meines Lebens in dieser Gegend, die für solche Seelen geschaffen ist wie die meine.";

$lang['pressconference_content_header'] = "pressekonferenz";
$lang['pressconference_content'] = "";

$lang['lounge_content_header'] = "lounge";
$lang['lounge_content'] = "";

$lang['bar_content_header'] = "bar";
$lang['bar_content'] = "";

$lang['dinner_content_header'] = "galadinner";
$lang['dinner_content'] = "";

$lang['rooftop_content_header'] = "dachgarten";
$lang['rooftop_content'] = "";

$lang['christmas_content_header'] = "weihnachtsfeier";
$lang['christmas_content'] = "";

$lang['home_content_menu_download'] = "Factsheet";
$lang['home_content_menu_pictures'] = "Bilder";
$lang['home_content_menu_press'] = "Presse";
$lang['home_content_menu_contact'] = "Kontakt";
$lang['home_content_menu_impress'] = "Impressum";
$lang['home_content_menu_campaign'] = "Kampagne";

$lang['footer_text'] = "k47.wien • Franz-Josefs-Kai 47 • 1010 Wien • +43 1 22 600 28 • <a href='mailto:office@k47.wien'>office@k47.wien</a>";


$lang['location_factsheet'] = "k47.wien Locationpräsentation";
$lang['location_filename'] = "k47.wien_presentation_location_de.pdf";

$lang['location_factsheet2'] = "k47.wien EM Lounge";
$lang['location_filename2'] = "k47.wien_EM_Lounge.pdf";
