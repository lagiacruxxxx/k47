<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class K47 extends CI_Controller 
{
	protected $language = null;
	protected $is_mobile = null;
	protected $contentmenu = null;
	protected $lang_switch = null;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->language = $this->getLang();
		$this->lang->load('frontend', $this->language);
		$this->is_mobile = $this->agent->is_mobile();
		$this->contentmenu = $this->load->view('frontend/content_menu', array(), true);
		$this->lang_switch = $this->load->view('frontend/lang_switch', array(), true);
	}
	
	protected function getLang()
	{
		if($this->input->cookie('k47_lang'))
		{
			$lang = $this->input->cookie('k47_lang');
		}	
		else 
			$lang = "german";
		
		return $lang;
	}
	
	public function add_email()
	{
		$email = mysql_real_escape_string($_POST['contact_email']);
		$name = mysql_real_escape_string($_POST['contact_name']);
		$phone = mysql_real_escape_string($_POST['contact_phone']);

		$this->load->model('main_model');
		$this->main_model->saveEmail($email,$name,$phone);
		
		
		$this->load->library('MY_mailer');
		$this->my_mailer->setMySender('requests@k47.wien', 'Anfragen k47.wien');
		$this->my_mailer->AddAddress('c.marko@k47.wien'); 
		$this->my_mailer->Subject = 'Neue Anfrage';
		$this->my_mailer->Body = 'Es ist eine neue Anfrage eingetroffen: Email: '.$email.', Name: '.$name.', Telefonnummer: '.$phone;
		
		$this->my_mailer->Send();
		
		echo json_encode(array('success' => true,
							   'message' => 'Danke für ihr Interesse. Sie erhalten sobald als möglich die gewünschten Unterlagen.'));			
	}	
	
	
	public function index()
	{
		$data['contentmenu'] = $this->contentmenu;
		$data['lang_switch'] = $this->lang_switch;
		
		$data['is_mobile'] = $this->is_mobile;
		
		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/home', $data);
		$this->load->view('frontend/footer', $data);
	}
	
	public function pressconference()
	{
		$data['contentmenu'] = $this->contentmenu;
		$data['lang_switch'] = $this->lang_switch;

		$data['is_mobile'] = $this->is_mobile;

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/pressconference', $data);
		$this->load->view('frontend/footer', $data);
		
	}	
	
	public function lounge()
	{
		$data['contentmenu'] = $this->contentmenu;
		$data['lang_switch'] = $this->lang_switch;

		$data['is_mobile'] = $this->is_mobile;

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/lounge', $data);
		$this->load->view('frontend/footer', $data);
		
	}		
	
	public function bar()
	{
		$data['contentmenu'] = $this->contentmenu;
		$data['lang_switch'] = $this->lang_switch;

		$data['is_mobile'] = $this->is_mobile;

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/bar', $data);
		$this->load->view('frontend/footer', $data);
		
	}		

	public function dinner()
	{
		$data['contentmenu'] = $this->contentmenu;
		$data['lang_switch'] = $this->lang_switch;

		$data['is_mobile'] = $this->is_mobile;
	
		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/dinner', $data);
		$this->load->view('frontend/footer', $data);
	}
	
	public function rooftop()
	{
		$data['contentmenu'] = $this->contentmenu;
		$data['lang_switch'] = $this->lang_switch;

		$data['is_mobile'] = $this->is_mobile;
	
		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/rooftop', $data);
		$this->load->view('frontend/footer', $data);
	}	
	
	public function christmas()
	{
		$data['contentmenu'] = $this->contentmenu;
		$data['lang_switch'] = $this->lang_switch;

		$data['is_mobile'] = $this->is_mobile;
	
		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/christmas', $data);
		$this->load->view('frontend/footer', $data);
	}		




	public function contact()
	{
		$data['language'] = $this->language;
		$data['is_mobile'] = $this->agent->is_mobile();
		
		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/contact', $data);
		$this->load->view('frontend/footer', $data);
	}
	
	public function press()
	{
		$data['is_mobile'] = $this->agent->is_mobile();

		$this->load->model('main_model');
		
		$data['pressitems'] = $this->main_model->getPressItemsByLang($this->language);
		
		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/press', $data);
		$this->load->view('frontend/footer', $data);	
	}
	
	public function pictures()
	{
		$data['is_mobile'] = $this->agent->is_mobile();

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/pictures', $data);
		$this->load->view('frontend/footer', $data);	
	}	
	
	public function download()
	{
		$data['is_mobile'] = $this->agent->is_mobile();

		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/download', $data);
		$this->load->view('frontend/footer', $data);		
	}
	

	
	public function campaign()
	{
		$data['is_mobile'] = $this->agent->is_mobile();
		$data['language'] = $this->language;
		$this->load->view('frontend/header', $data);
		$this->load->view('frontend/campaign', $data);
		$this->load->view('frontend/footer', $data);		
	}	

	public function swf_test()
	{
		$this->load->view('swf_test', array());
	}
}

