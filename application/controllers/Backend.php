<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backend extends CI_Controller {

    function __construct()
    {
        parent::__construct();
		
		
	    $this->load->library('image_CRUD');
        $this->load->library('ion_auth');
		$this->load->library('grocery_CRUD');
        
    }
 
    public function index()
    {
		if($this->ion_auth->logged_in())
		{
			$this->load->view('backend/header.php');	
			$this->load->view('backend/footer.php');	
		}
		else redirect ('auth/login');
    }   
	
	public function pressitems()
	{
		if($this->ion_auth->logged_in())
		{
			$crud = new grocery_CRUD();
			$crud->set_table('press_items');
			$crud->set_subject('Presseartikel');
			$crud->columns('headline', 'lang', 'image', 'video_embedd');
			$crud->add_fields('headline', 'lang', 'content', 'image', 'video_embedd');
			$crud->edit_fields('headline', 'lang', 'content', 'image', 'video_embedd');
			
			$crud->unset_texteditor('content');
			$crud->field_type('lang', 'enum', array('german', 'english'));
			
			$crud->display_as('headline','Überschrift');
			$crud->display_as('lang','Sprache');
			$crud->display_as('content','Text Inhalt');
			$crud->display_as('image','Bild (optional)');
			$crud->display_as('video_embedd','Youtube-code (optional)');
			
            $crud->set_field_upload('image','items/general/uploads');
			
			$crud->callback_after_upload(array($this,'create_thumb'));
			
            $output = $crud->render();
			$this->load->view('backend/default.php',$output);
		}
		else $this->js_logout();
	}
	
	
	public function pressitem_ordering($lang)
	{
		if($this->ion_auth->logged_in())
		{
			$this->load->model('main_model');
			$data['title'] = "Artikelreihenfolge " . $lang;
			$data['items'] = $this->main_model->getPressItemsByLang($lang);
			
			$html['output'] = $this->load->view('backend/pressitem_ordering', $data, true);
			$html['css_files'] = array(site_url('items/backend/css/basic.css'));
			$html['js_files'] = array(	site_url('items/backend/js/jquery.js'), 
										site_url('items/backend/js/jquery-ui-1.10.4.custom.min.js'),
										site_url('items/backend/js/ordering.js'));
			
			$this->load->view('backend/default.php',$html);
		}
		else $this->js_logout();					
	}
    

	public function save_ordering()
	{
		$content = json_decode(file_get_contents('php://input'));
		$col = array();
		
		$success = true;
		$message = "Ordering gespeichert";
		
		$this->load->model('main_model');
		
		foreach($content as $item)
		{
			$item_id = $item->item_id;
			$ordering = $item->ordering;
			
			$subsites[] = array('id' => $item_id,
						   	   	'priority' => $ordering);
		}
		
		if($this->main_model->saveOrdering($subsites, 'id') <= 0)
		{
			$success = false;
			$message = "Fehler beim Speichern.";
		}		
		
		echo json_encode(array('success' => $success,
							   'message' => $message));
	}	
	
	public function create_thumb($uploader_response, $field_info, $files_to_upload)
	{
		
		echo $field_info->upload_path . '/thumb/' . $uploader_response[0]->name;
		
		$this->load->library('image_moo');
	 
	    //Is only one file uploaded so it ok to use it with $uploader_response[0].
	    $file_uploaded = $field_info->upload_path.'/'.$uploader_response[0]->name; 
	 
	    $this->image_moo->load($file_uploaded)->resize(500,350)->save($field_info->upload_path . '/thumb/' . $uploader_response[0]->name, true);
	 
	    return true;		
	}
	
   
	function js_logout()
	{

		echo "<script>";
		echo "parent.location.href='".site_url('auth/login')."'";
		echo "</script>";	
	}
}

/* End of file backend.php */
/* Location: ./application/controllers/backend.php */
