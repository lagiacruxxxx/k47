

			<script> var slidercount = 0</script>

			
			<div class="contact">
				<? if($language == 'german'): ?>
					<span class="contact_headline">Der Kubus neu inszeniert</span><br>
					<br>
					Inszeniert wurde das neue k47.wien in einem aufwendigen Day &amp; Night Shooting vom österreichischen Starfotografen Michael Dürr.<br>
					<br>  
					Das Briefing an den Fotografen lautete die diversen „Eventformate“ wie, Pressekonferenz, Präsentation, Cocktailempfang, Seminar, Kongress oder Galadinner, stilgerecht in Szene zu setzen und für die aktuelle Imagekampagne zu shooten.<br>
					<br>
					Das aufwendige 24-Stunden-Shooting mit 25 Personen am Set und sieben Models fand sowohl bei Tag wie auch bei Nacht statt und setzte den „Look & Feel“ des k47.wien für die neue Imagekampagne ins Licht. „Die Architektonik mit seinen klaren Schnitten, Spiegelungen und Lichtbrechungen ist natürlich ein Traumobjekt für mich als Fotograf“, so der international angesehene Mode- und Portraitfotograf Michael Dürr begeistert.<br>
					<br>
					<span class="contact_subheadline">Über Michael Dürr</span><br>
					<br>
					Der gebürtige Wiener Michael Dürr gilt seit über 20 Jahren als Spezialist für "Available Light" (Tageslichtfotografie) und kann auf zahlreiche nationale sowie internationale Auftrags-produktionen, Publikationen, Ausstellungsprojekte, Mode - und Werbekampagnen verweisen. Seine fotografische Handschrift hat mittlerweile einen hohen Wiedererkennungswert und ist bei Art-Direktoren und Mode-Redakteuren im In- und Ausland gefragt. So arbeitete er als Porträt & Modephotograph im Bereich Kunst, Mode und Design mit bekannten Namen wie Bless, Wendy & Jim, Petar Petrov, Claudia Rosa Lukas, Gregor Pirouzi, Nina Peter, Elfenkleid, Lena Hoschek / Robert La Roche, Palmers, Diesel, Cos, Mac Cosmetics und vielen anderen.<br> 
					<br>
					Bekannt wurde Michael Dürr auch mit seinem engagierten Kunstprojekt Cinema Photographique, Europas erstem temporären Kino für Porträt & Modefotografie. Um Fotografie nicht nur in Printmedien oder Internet sondern auch als Kinoerlebnis anzubieten organisiert der Porträt & Modefotograf Foto-Kino-installationen mit Musikbegleitung.<br> 
					<br>
					Die Ausstellungsreihe gastierte bereits an Orten wie dem Planetarium Kuppelsaal, WUK Museumssäle, Pratersauna Artspace, Museumsquartier Q21, Albertina Musensaal, Kunsthalle Project Space, Palais Liechtenstein, und vielen weiteren. Gezeigt wurden Mode- und Porträtaufnahmen von Diane Kruger, Vincent Gallo, Vivienne Westwood, Erwin Wurm, Adrien Brody, David Lynch oder Modesujets, die in Zusammenarbeit mit Swarovski, Diesel, Mac Cosmetics oder österreichischen Modedesignern entstanden.<br>
					<br>
					Vielen Dank an unsere Partner:
				<? else: ?>
					<span class="contact_headline">The newly staged cube</span><br>
					<br>
					The new k47.wien has been staged at an elaborate day and night shoot by Austria’s star photographer Michael Dürr.<br>
					<br>  
					The brief to the photographer was to stylishly stage various “event formats” such as a press conference, presentation, cocktail reception, seminar, congress or gala dinner and shoot them for the current image campaign.<br>
					<br>
					The elaborate 24-hour shoot with 25 people on the set and seven models took place both during the day and at night and highlighted the look and feel of k47.wien for the new image campaign. “The architecture with its clean cut lines, reflections and refractions is of course a marvellous setting for me as a photographer”, says the internationally renowned fashion and portrait photographer Michael Dürr enthusiastically.<br>
					<br>
					<span class="contact_subheadline">About Michael Dürr</span><br>
					<br>
					Born in Vienna, Michael Dürr has been considered a specialist in available light or daylight photography for over 20 years and is able to look back on numerous national and international commissions, publications, exhibition projects, and fashion and advertising campaigns. His photographic signature has achieved great recognition value these days and is much in demand among art directors and fashion editors at home and abroad. He has worked as a portrait and fashion photographer in the world of art, fashion and design with well-known names such as Bless, Wendy & Jim, Petar Petrov, Claudia Rosa Lukas, Gregor Pirouzi, Nina Peter, Elfenkleid, Lena Hoschek / Robert La Roche, Palmers, Diesel, Cos, Mac Cosmetics and many others.<br>
					<br>
					Michael Dürr has also become well known with his commitment to the art project Cinema Photographique, Europe’s first temporary cinema for portrait and fashion photography. In order to provide photography not only in print media or on the Internet but also as a cinematic experience, the portrait and fashion photographer organises photo-cinema installations with musical accompaniment.<br>
					<br>
					The exhibition series has already appeared at venues such as the Planetarium Kuppelsaal, WUK museum halls, Pratersauna Artspace, Museumsquartier Q21, Albertina Musensaal, Kunsthalle Project Space, Palais Liechtenstein, and many others. It has shown fashion and portrait shots of Diane Kruger, Vincent Gallo, Vivienne Westwood, Erwin Wurm, Adrien Brody, David Lynch or fashion subjects that have been created in collaboration with Swarovski, Diesel, Mac Cosmetics or Austrian fashion designers.<br>
					<br>
					Many thanks to our partners:			
				<?endif; ?> 
				BOSS, 
				Ciro, 
				COS, 
				Diesel, 
				Elisabetta Franchi, 
				ep_anoui by Eva Poleschinski, 
				Humanic, 
				Luly Yang via Popp &amp; Kretschmer,
				Missoni,
				Peek &amp; Cloppenburg, 
				Polaroid, 
				Sonia Rykiel, 
				Tommy Hilfiger, 
				Vivienne Westwood,
				Wolford
			</div>
			
