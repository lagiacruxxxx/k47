
		<div>
			<div class="menuitem">
				<span class="menuarrow">
					<img src="<?= site_url('items/frontend/img/arrow.png')?>" class="hidden">
				</span>
				<a  style="text-decoration:none;" href="<?= site_url('download')?>">
					<?= $this->lang->line('home_content_menu_download')?>
				</a>
			</div>
			<div class="menuitem">
				<span class="menuarrow">
					<img src="<?= site_url('items/frontend/img/arrow.png')?>" class="hidden">
				</span>
				<a style="text-decoration:none;" href="<?= site_url('pictures')?>">
					<?= $this->lang->line('home_content_menu_pictures')?>
				</a>
			</div>
			<div class="menuitem">
				<span class="menuarrow">
					<img src="<?= site_url('items/frontend/img/arrow.png')?>" class="hidden">
				</span>
				<a style="text-decoration:none;" href="<?= site_url('campaign')?>">
					<?= $this->lang->line('home_content_menu_campaign')?>
				</a>
			</div>
			<div class="menuitem">
				<span class="menuarrow">
					<img src="<?= site_url('items/frontend/img/arrow.png')?>" class="hidden">
				</span>
				<a style="text-decoration:none;" href="<?= site_url('contact')?>">
					<?= $this->lang->line('home_content_menu_contact')?>
				</a>
			</div>
			<div class="menuitem">
				<span class="menuarrow">
					<img src="<?= site_url('items/frontend/img/arrow.png')?>" class="hidden">
				</span>
				<a style="text-decoration:none;" href="<?= site_url('press')?>">
					<?= $this->lang->line('home_content_menu_press')?>
				</a>
			</div>
		</div>
		