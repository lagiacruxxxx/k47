<script>var slidercount = 0;</script>

		<div class="presscontent">
			<?php $i = 1;  foreach($pressitems->result() as $item): ?>
				<div class="pressitem <?php if($i % 2 == 0) echo " pressitem_right";?>">
					<div class="press_header">
						<?= $item->headline ?>
					</div>
					<? if($item->video_embedd != ""):?>
					<div class="press_video">
						<iframe class="press_video_iframe" src="<?= 'https://www.youtube.com/embed/' . $item->video_embedd . '?rel=0&amp;wmode=transparent'?>" frameborder="0" allowfullscreen="" wmode="Opaque"></iframe>
					</div>	
					<? endif;?>
					<? if($item->image != ""): ?>
						<div class="press_image ">
							<a href="<?= site_url('items/general/uploads/' . $item->image);?>" data-lightbox="<?= $i?>" data-title="<?= $item->headline ?>">
								<img src="<?= site_url('items/general/uploads/' . $item->image);?>">
							</a>
						</div>
					<? endif; ?>
					<div class="press_content">
						<?= $item->content ?>
					</div>
				</div>
			<?php $i++; endforeach; ?>
		</div>
