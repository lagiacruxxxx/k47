
<script> var slidercount = 0</script>

			<div class="contact">
			<? if($language == 'german'): ?>
				<span class="contact_headline">KONTAKT</span><br>
				<br>
				k47.wien, day &amp; night lounge<br>
				Franz-Josefs-Kai 47, penthouse<br>
				1010 Wien<br>
				<br>
				<a href='mailto:office@k47.wien'>office@k47.wien</a><br>
				01/226 00 28<br>
				<a href="http://www.k47.wien">www.k47.wien</a><br>
				<!--<a href="https://www.facebook.com/k47Wien" target="_blank">facebook.com/k47Wien</a><br>-->
				<br>
				<br>
				<span class="contact_headline">ERREICHBARKEIT</span><br>
				<br>
				<iframe id="maps_iframe" src="https://www.google.com/maps/embed?pb=!1m16!1m10!1m3!1d10634.564918951002!2d16.372874401607543!3d48.2135260229278!2m1!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x476d07a47f83f53d%3A0x8d4879e35de2c03a!2sFranz-Josefs-Kai+47%2C+1010+Wien!5e0!3m2!1sde!2sat!4v1413292992384"  frameborder="0" style="border:0"></iframe><br>
				<br>			
				Öffentliche Verkehrsmitteln unmittelbar vor dem k47.wien<br>
				<br>
				U2 Schottenring<br>
				U4 Schottenring<br>
				Strassenbahnlinie 1, 2, 31, D<br> 
				Bus 3a<br>
				<br>
				<br>
				<br>
				<span class="contact_headline">IMPRESSUM</span><br>
				<br>
				<span class="contact_subheadline">Medieninhaber</span><br>
				OL Betriebs GmbH<br>
				Wienerbergstrasse 11<br>
				1100 Wien<br>
				<br>
				<span class="contact_subheadline">Firmenbuchnummer</span><br>
				FN 377028d<br>
				<br>
				Handelsgericht Wien<br>
				<br>
				<span class="contact_subheadline">Firmensitz</span><br>
				in politischer Gemeinde Wien<br>
				<br>
				UID ATU67644757<br>
				<br>
				Copyrighthinweise<br>
				© OL Betriebs GmbH, 2014
			<? else: ?>
				<span class="contact_headline">CONTACT</span><br>
				<br>
				k47.wien, day &amp; night lounge<br>
				Franz-Josefs-Kai 47, penthouse<br>
				1010 Vienna<br>
				<br>
				<a href='mailto:office@k47.wien'>office@k47.wien</a><br>
				01/226 00 28<br>
				<a href="http://www.k47.wien">www.k47.wien</a><br>
				<!--<a href="https://www.facebook.com/k47Wien" target="_blank">facebook.com/k47Wien</a><br>-->
				<br>
				<br>
				<span class="contact_headline">DIRECTIONS</span><br>
				<br>
				<iframe id="maps_iframe" src="https://www.google.com/maps/embed?pb=!1m16!1m10!1m3!1d10634.564918951002!2d16.372874401607543!3d48.2135260229278!2m1!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x476d07a47f83f53d%3A0x8d4879e35de2c03a!2sFranz-Josefs-Kai+47%2C+1010+Wien!5e0!3m2!1sde!2sat!4v1413292992384"  frameborder="0" style="border:0"></iframe><br>
				<br>			
				Public transport connections at k47.wien<br>
				<br>
				U2 Schottenring<br>
				U4 Schottenring<br>
				Tram line 1, 2, 31, D<br> 
				Bus 3a<br>
				<br>
				<br>
				<br>
				<span class="contact_headline">LEGAL NOTICE</span><br>
				<br>
				<span class="contact_subheadline">Proprietor</span><br>
				OL Betriebs GmbH<br>
				Wienerbergstrasse 11<br>
				1100 Vienna<br>
				<br>
				<span class="contact_subheadline">Company register number</span><br>
				FN 377028d<br>
				<br>
				Commercial Court of Vienna<br>
				<br>
				<span class="contact_subheadline">Head office</span><br>
				in the municipality of Vienna<br>
				<br>
				UID ATU67644757<br>
				<br>
				Copyright<br>
				© OL Betriebs GmbH, 2014			
			<?endif; ?> 	
			</div>

	