
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html>
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta charset="utf-8">
		<meta property="og:title" content="k47"/>
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="<?= site_url();?>"/>
		<meta property="og:image" content="<?=site_url("items/img/logo.png"); ?>"/>
		<meta property="og:description" content=""/>
		<meta name="description" content=""/>
		<meta name="google" content="notranslate" />
        <title>K47</title>

		<!------------- VARS ----------->
        <script>
            var rootUrl = "<?= site_url(); ?>";
            var is_mobile = "<?= $is_mobile?>";
        </script>

		<!------------- CSS & JS ----------->
        <script src="<?= site_url("/items/general/js/jquery-2.1.1.min.js");?>" type="text/javascript"></script>
        <script src="<?= site_url("items/frontend/js/swfobject.js"); ?>" type="text/javascript"></script>
        <script src="<?= site_url("items/frontend/js/szilagyi.js"); ?>" type="text/javascript"></script>
        <script src="<?= site_url("items/frontend/js/jquery.easing.1.3.js"); ?>" type="text/javascript"></script>
        
        <script src="<?= site_url("items/frontend/js/lightbox.js"); ?>" type="text/javascript"></script>
        
        <link href="<?= site_url("items/frontend/css/basic.css"); ?>" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?= site_url("items/frontend/css/lightbox.css"); ?>" rel="stylesheet" type="text/css" media="screen" />
           
        <!------------- GA ----------->    
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-5934189-50', 'auto');
		  ga('send', 'pageview');
		
		</script>	
		
	</head>
	<body>
		<!--<img src="<?= site_url('items/frontend/img/background.png')?>" class="background_bubbles">-->
		<div class="wrapper">
			<div class="header">
				<div class="headerlogo">
					<a href="<?= site_url()?>">
						<img src="<?= site_url('items/frontend/img/logo.png')?>">
					</a>
				</div>				
				<div class="headermenu">
					<ul>
						<li class="menu">
							<span class="menuarrow mainmenuarrow">
								<img src="<?= site_url('items/frontend/img/arrow.png')?>" class="hidden">
							</span>
							<span>
								<a href="<?= site_url('')?>">
									<?=$this->lang->line('menupoint_home')?>
								</a>	
							</span>
						</li>
						<li class="menu">
							<span class="menuarrow mainmenuarrow">
								<img src="<?= site_url('items/frontend/img/arrow.png')?>" class="hidden">
							</span>
							<span>
								<a href="<?= site_url('pressconference')?>">
									<?=$this->lang->line('menupoint_pressconference')?>
								</a>	
							</span>
						</li>
						<li class="menu">
							<span class="menuarrow mainmenuarrow">
								<img src="<?= site_url('items/frontend/img/arrow.png')?>" class="hidden">
							</span>
							<span>
								<a href="<?= site_url('lounge')?>">
									<?=$this->lang->line('menupoint_lounge')?>
								</a>
							</span>
						</li>						
						<li class="menu">
							<span class="menuarrow mainmenuarrow">
								<img src="<?= site_url('items/frontend/img/arrow.png')?>" class="hidden">
							</span>
							<span>
								<a href="<?= site_url('bar')?>">
									<?=$this->lang->line('menupoint_bar')?>
								</a>
							</span>
						</li>						
						<li class="menu">
							<span class="menuarrow mainmenuarrow">
								<img src="<?= site_url('items/frontend/img/arrow.png')?>" class="hidden">
							</span>
							<span>
								<a href="<?= site_url('dinner')?>">
									<?=$this->lang->line('menupoint_dinner')?>
								</a>
							</span>
						</li>						
						<li class="menu">
							<span class="menuarrow mainmenuarrow">
								<img src="<?= site_url('items/frontend/img/arrow.png')?>" class="hidden">
							</span>
							<span>
								<a href="<?= site_url('rooftop')?>">
									<?=$this->lang->line('menupoint_rooftop')?>
								</a>
							</span>
						</li>						
						<li class="menu">
							<span class="menuarrow mainmenuarrow">
								<img src="<?= site_url('items/frontend/img/arrow.png')?>" class="hidden">
							</span>
							<span>
								<a href="<?= site_url('christmas')?>">
									<?=$this->lang->line('menupoint_christmas')?>
								</a>
							</span>
						</li>						
					</ul>
				</div>

			</div>
	