<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>K47 Backend</title>
    <link href="<?=site_url("items/backend/css/basic.css") ?>" rel="stylesheet" type="text/css" media="screen" />
	<link rel="stylesheet" type="text/css"  media="screen" href="<?php echo(site_url("assets/grocery_crud/themes/flexigrid/css/flexigrid.css")); ?>" id="StyleCSS"/>
	<script type="text/javascript" src="<?=site_url("items/backend/js/jquery.js");?>"></script> 

	<script>
		var rootUrl='<?=site_url();?>';
	</script>
	<script type="text/javascript" src="<?=site_url("items/backend/js/backend.js");?>"></script> 
</head>
<script>
$(document).ready(function() {
  $('#opener').trigger('click');
  //$('.menuitem[section=screen1]').trigger('click');
  
});
	
</script>

<body>
	
	<div id="menutop">
		<div style="position: absolute; top: 0px; height: 100%; overflow: hidden; width: 100%; z-index: 1;">
				<img src="<?=site_url('items/backend/img/bg.jpg');?>" width="100%"/>
		</div>	

				
	</div>
	<div id="opener">
		<p style="text-align: center; color: white; z-index: 1100; position: absolute; top: -30px; width: 100%; left: -10px;">MENÜ</p>
		<img src="<?=site_url('items/backend/img/opener.png');?>" />
		</div>
	<div id="wrapper">
		<div id="menu">
			<p class="menuitem" section="screen2" src="<?=site_url('backend/pressitems');?>">Presseartikel</a>
			<p class="menuitem" section="screen2" src="<?=site_url('backend/pressitem_ordering/german');?>">Presseartikel Reihenfolge DE</a>
			<p class="menuitem" section="screen2" src="<?=site_url('backend/pressitem_ordering/english');?>">Presseartikel Reihenfolge EN</a>
			<p class="menuitem" section="logout" src="<?=site_url('auth/logout');?>">LOGOUT</a>
			
		</div>	
		<div id="content" >
			<iframe id="crud" src="" height="90%" width="100%" frameborder="0" ></iframe>
		</div>

