<?php

class main_model extends CI_Model  
{
	public function getPressItems()
	{
		return $this->db->get('press_items');
	}
	
	public function getPressItemsByLang($lang)
	{
		$this->db->where('lang', $lang);
		$this->db->order_by('priority', 'asc');
		return $this->db->get('press_items');
	}
	
	public function saveEmail($email,$name,$phone)
	{
		$this->db->insert('email_requests', array(	'email' => $email,
													'name' => $name,
													'phone' => $phone,
													'status' => EMAIL_STATUS_PENDING));
	}
	
	function saveOrdering($array, $key)
	{
		$this->db->update_batch('press_items', $array, $key);
		return $this->db->affected_rows();
	}	
}
?>