<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['my_mailer_IsSMTP'] = true; 
$config['my_mailer_SMTPAuth'] = true;
$config['my_mailer_SMTPSecure'] = "ssl";
$config['my_mailer_Host'] = "smtp.easyname.eu";
$config['my_mailer_Port'] = 465;
$config['my_mailer_Username'] = "1590mail18";
$config['my_mailer_Password'] = "iUYXyFbwCvAYw";
$config['my_mailer_From'] = array("email" => "no-reply-denk@uniqa.at",
								  "name" => "Denk UNIQA");
$config['my_mailer_ReplyTo'] = array("email" => "no-reply-denk@uniqa.at",
								     "name" => "Denk UNIQA");

$config['my_mailer_substitutes'] = array(
										'::firstname::' => array(	'type' => 'db',
																 	'table' => 'users_view',
																 	'target' => 'firstname',
																 	'key' => 'id',
																 	'value' => 'user_id',
																	'description' => 'my_mailer_desc_firstname'),
																 	
										'::lastname::' => array(	'type' => 'db',
																	'table' => 'users_view',
																	'target' => 'lastname',
																	'key' => 'id',
																	'value' => 'user_id',
																	'description' => 'my_mailer_desc_lastname'),
																	
										'::reason::' => array(		'type' => 'param',
																	'param' => 'reason',
																	'description' => 'my_mailer_desc_reason'),
																	
										'::activationlink::' => array(	'type' => 'param',
																		'param' => 'activationlink',
																	'description' => 'my_mailer_desc_activationlink'),
																	
										'::password::' => array(	'type' => 'param',
																	'param' => 'password',
																	'description' => 'my_mailer_desc_password'));

$config['my_mailer_log_table'] = "email_log";
