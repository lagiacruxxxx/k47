<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once('PHPMailer/class.phpmailer.php'); 

class MY_Mailer extends PHPMailer
{
	private $email_template_model = null;
	private $users_model = null;
	private $mail_type = null;
	private $substitutes = null;
	private $user_id = null;
	private $country_id = null;
	private $my_mailer_model = null;
	private $params = null;
	private $lang = null;
	private $config = null;
	private $log_emails = false;
	private $log_table = null;
	
	
	/***************************************************************************************
	 * INFO: constructor reads the my_mailer config file and initializes the class
	 * PARAMS: ---
	 ***************************************************************************************/
    public function __construct()
    {
		parent::__construct(true);

		$ci = &get_instance();
		
		$this->lang = $ci->lang;
		
		$ci->load->config('my_mailer');
		$this->config = $ci->config;
		if($ci->config->item('my_mailer_IsSMTP'))
		{
			$this->IsSMTP();
			$this->SMTPAuth = $ci->config->item('my_mailer_SMTPAuth');
			$this->SMTPSecure = $ci->config->item('my_mailer_SMTPSecure');
			$this->Host = $ci->config->item('my_mailer_Host');
			$this->Port = (int)$ci->config->item('my_mailer_Port');
			$this->Username = $ci->config->item('my_mailer_Username');
			$this->Password = $ci->config->item('my_mailer_Password');
		}	
		else 
		{
			// TODO: INSERT CODE FOR POP3
		}
		
		if($ci->config->item('my_mailer_log_table') != "")
		{
			$this->log_emails = true;
			$this->log_table = $ci->config->item('my_mailer_log_table');
		}
		
		$from = $ci->config->item('my_mailer_From');
		$this->SetFrom($from['email'], $from['name']);
		$replyto = $ci->config->item('my_mailer_ReplyTo');
		$this->AddReplyTo($replyto['email'], $replyto['name']);
		
	
		$this->CharSet = "UTF-8";
		$this->isHTML(true);
    }
	
	/***************************************************************************************
	 * INFO: set functions
	 * PARAMS: ---
	 ***************************************************************************************/
	public function setMailType($mail_type = null)
	{
		$this->mail_type = $mail_type;
		
		return $this->mail_type;
	}	
	public function setUser($user_id = null)
	{
		$this->user_id = $user_id;
	
		$ci = &get_instance();
		$ci->load->model('backend_entities/users_model');
		
		$this->country_id = $ci->users_model->getCountry($user_id);
		
		return $this->user_id;
	}
	public function setParams($params = null)
	{
		$this->params = $params;
		
		return $this->params;
	}
	
	public function setMySender($sender_email, $sender_name)
	{
		$this->SetFrom($sender_email, $sender_name);
	}

	/***************************************************************************************
	 * INFO: sends the mail corresponding to the chosen template 
	 * PARAMS: ---
	 ***************************************************************************************/
	public function sendFromTemplate()
	{
		$this->Subject = $this->getSubject();
		$this->Body = $this->getBody();
		$this->AltBody = $this->getAltBody();
		
		$ret = $this->Send();
		//$ret = true;
		if($ret && $this->log_emails)
			$this->logEmail();
		
		return $ret;
	}
	
	/***************************************************************************************
	 * INFO: reads the subject
	 * PARAMS: ---
	 ***************************************************************************************/
	private function getSubject()
	{
		return $this->email_template_model->getSubject($this->mail_type, $this->country_id);
	}
	
	
	/***************************************************************************************
	 * INFO: creates the mail body in conjunction with the delivered parameters 
	 * PARAMS: ---
	 ***************************************************************************************/
	private function getBody()
	{
		$body = $this->email_template_model->getBody($this->mail_type, $this->country_id);
		foreach($this->substitutes as $key => $value)
		{
			if(strpos($body, $key) !== false)
			{
				switch($value['type'])
				{
					case 'db':
						$replace = $this->my_mailer_model->getReplaceValue($value['table'], $value['target'], $value['key'], $this->$value['value']);
						break;
					case 'param':
						$replace = $this->params[$value['param']];
						if($value['param'] == "activationlink")
						{
							$replace = '<a href="' . $replace . '">' . $replace . '</a>';
						}
						break;
				}	
				
				$body = str_replace($key, $replace, $body);
			}
		}
		return $body;
	}
	
	/***************************************************************************************
	 * INFO: creates a html-less version of the body
	 * PARAMS: ---
	 ***************************************************************************************/
	private function getAltBody()
	{
		return strip_tags($this->getBody());
	}
	
	
	/***************************************************************************************
	 * INFO: creates a table with all possible substitute values 
	 * PARAMS: 	$view = view that shows the table
	 * 			$return = return view content as string
	 ***************************************************************************************/
	public function getSubstituteTable($view = "my_mailer_substitutes", $return = false)
	{
		
		foreach($this->config->item('my_mailer_substitutes') as $key => $value)
		{
			$data['substitutes'][] = array('substitute' => $key,
										   'value' => $this->lang->line($value['description']));
		}
		
		$data['header_shortcut'] = $this->lang->line('my_mailer_header_shortcut');
		$data['header_replace'] = $this->lang->line('my_mailer_header_replace');
		
		$ci = &get_instance();
		$content = $ci->load->view($view, $data, $return);
		
		if($return)
			return $content;	 	
	}
	
	
	/***************************************************************************************
	 * INFO: logs the email in the defined table
	 * PARAMS: --- 	
	 ***************************************************************************************/
	private function logEmail()
	{
		$toArr = array();
        foreach ($this->to as $t) {
            $toArr[] = $this->addrFormat($t);
        }
        $tos = implode(', ', $toArr);
		
		$ccs = "";
		/*foreach($this->cc as $key => $value)
		{
			$ccs = $this->cc[$key] . '|';
		}*/
		
		$ci = &get_instance();
		$ci->load->model('my_mailer_model');
		$ci->my_mailer_model->logEmail($this->log_table,
										$this->Subject,
										$tos,
										$ccs,
										$this->From,
										$this->Body,
										$this->AltBody);
	}
}