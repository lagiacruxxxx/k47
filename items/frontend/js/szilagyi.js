var pic_count;

$(document).ready(function()
{
	getFlash();
	if(slidercount > 0)
		addSliderListeners();
	addListeners();
	
	resize();
	
	initialiseSliders();
	bindSliderArrows();
	pic_count = $('.slideritem').length;
});


function addListeners()
{
	$('.menuitem, .menu').hover(function()
	{
		$(this).find('.menuarrow').children('img').removeClass('hidden');
	},
	function()
	{
		$(this).find('.menuarrow').children('img').addClass('hidden');
	});
	
	$('#contactform').submit(function(event)
	{
		event.preventDefault();
		$.ajax(
		{
			url: rootUrl + 'add_email',
			data: $('#contactform').serializeArray(),
			method: 'POST',
			success: function(data)
			{
				ret = $.parseJSON(data);
				if(ret.success)
				{
					$('#contactform')[0].reset();
					$('.contact_error').text(ret.message);
				}
			}
		});	
	});
	
	$('.lang').click(function()
	{
		docCookies.setItem('k47_lang', $(this).attr('lang'), Infinity, null, null , false);
		location.reload(true);
	});
	
	$('.slider2').mouseenter(function()
	{
		$(this).find('.arrow').fadeIn(200);
	});
	
	$('.slider2').mouseleave(function()
	{
		$(this).find('.arrow').fadeOut(200);
	});	
	

}


function bindSliderArrows()
{
	$('.arrow_left').click(function()
	{
		unbindSliderArrows();
		$('.slideritem[order="' + (pic_count-1) + '"]').css({'left' : '-100%'});
		
		$('.slideritem').animate({left: '+=100%'}, 2000, 'easeInOutQuad', function()
		{
			var order = $(this).attr('order');
			order++;
			if(order > (pic_count-1))
			{
				$(this).attr('order', 0);
				$(this).css({left: '0%'});
			}
			else
			{
				$(this).attr('order', order);
			}
		});
		setTimeout('bindSliderArrows()', 2050);			
	});

	$('.arrow_right').click(function()
	{
		unbindSliderArrows();
		$('.slideritem').animate({left: '-=100%'}, 2000, 'easeInOutQuad', function()
		{
			var order = $(this).attr('order');
			order--;
			if(order < 0)
			{
				$(this).attr('order', pic_count-1);
				$(this).css({left: ((pic_count-1)*100) + '%'});
			}
			else
			{
				$(this).attr('order', order);
			}
		});
		setTimeout('bindSliderArrows()', 2050);		
	});	
}

function unbindSliderArrows()
{
	$('.arrow_left').unbind('click');
	$('.arrow_right').unbind('click');
}

function initialiseSliders()
{
	$('.slideritem').each(function()
	{
		var order = $(this).attr('order');
		$(this).css({left: (order * 100) + '%'});
	});
}

function addSliderListeners()
{
	setInterval(function()
	{
		$('.slideritem').animate({left: '-=100%'}, 2000, 'easeInOutQuad', function()
		{
			var order = $(this).attr('order');
			order--;
			if(order < 0)
			{
				$(this).attr('order', slidercount-1);
				$(this).css({left: ((slidercount-1)*100) + '%'});
			}
			else
			{
				$(this).attr('order', order);
			}
		});
	}, 7000);
}

$(window).resize(function()
{
	resize();
})


function resize()
{
	var wrapper = $('.wrapper');
	var windowWidth = $(window).width();
	
	if(windowWidth >= 875)
	{
		wrapper.css({width: '875px'});
		$('.menuitem').css({'float': 'left', 'line-height' : '1.5em', 'margin-left': '15px'});
		$('.contenttext').css({'float': 'left', 'margin-left': '0%'});
		$('.contentmenu').css({'float': 'left', 'margin-left': '0%'});
		$('.contact').css({'padding-left': '0%'});
		$('.headermenu').css({'margin-left': '0%', width: '50%'});
		$('.headerlogo').css({'margin-right': '0%', width: '50%'});
		$('.footer').css({width: '875px'});
		$('.lang_switch').css({'float': 'right', 'margin-left': '0%'});
	}
	else
	{
		wrapper.css({width: windowWidth + 'px'});
		$('.menuitem').css({'float': 'none', 'line-height': '1.5em', 'margin-left': '-11px'});
		$('.contenttext').css({'float': 'none', 'margin-left': '3%'});
		$('.contentmenu').css({'float': 'none', 'margin-left': '3%'});
		$('.contact').css({'padding-left': '3%'});
		$('.headermenu').css({'margin-left': '3%', width: '47%'});
		$('.headerlogo').css({'margin-right': '3%', width: '47%'});
		$('.footer').css({width: windowWidth});
		$('.lang_switch').css({'float': 'none', 'margin-left': '3%'});
	}

	var wrapperWidth = wrapper.width();
	$('#maps_iframe').css({width: wrapperWidth/2, height: parseInt(wrapperWidth/2*0.75)});
	var flashHeight = Math.round(wrapperWidth*0.56);
	$('#FlashHolder').css({height: flashHeight});
	$('.ImageHolder').css({height: flashHeight});

	if(windowWidth < 500)
	{
		$('.headerlogo').css({'float': 'none', width: '100%', 'text-align': 'left', 'line-height': '120px', 'margin-left': '3%'});
		$('.headermenu').css({'float': 'none', width: '100%', 'text-align': 'left', 'height': '120px'});
		$('.headermenu ul').css({'margin-top': '0px'});
		$('.header').css({'height': '240px'});
		$('.footer').css({'font-size': '7px'});
		$('.pressitem').css({width: '100%', 'margin-right': '0%'});
	}
	else
	{
		$('.headerlogo').css({'float': 'right', 'text-align': 'right', 'line-height': '150px', 'margin-left': '0%'});
		$('.headermenu').css({'float': 'right', 'text-align': 'left', 'height': 'auto'});
		$('.headermenu ul').css({'margin-top': '30px'});
		$('.header').css({'height': '150px'});
		$('.footer').css({'font-size': '9px'});
		$('.pressitem').css({width: '49%', 'margin-right': '1%'});
	}	
	
	$('.press_video').css({'height' : parseInt($('.pressitem').width()*0.625)});
	$('.press_image').css({'max-height' : parseInt($('.pressitem').width()*0.625)})
	
}

function getFlash() {	
	var flashvars = new Object;
	flashObject = swfobject.embedSWF(rootUrl+"items/frontend/swf/Player.swf", "FlashHolder", "100%", "100%", "9.0.0", "", flashvars, {wmode: 'opaque', bgcolor: '222222'});			
}


var docCookies = {
  getItem: function (sKey) {
    if (!sKey) { return null; }
    return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
  },
  setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
    if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
    var sExpires = "";
    if (vEnd) {
      switch (vEnd.constructor) {
        case Number:
          sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
          break;
        case String:
          sExpires = "; expires=" + vEnd;
          break;
        case Date:
          sExpires = "; expires=" + vEnd.toUTCString();
          break;
      }
    }
    document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
    return true;
  },
  removeItem: function (sKey, sPath, sDomain) {
    if (!this.hasItem(sKey)) { return false; }
    document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
    return true;
  },
  hasItem: function (sKey) {
    if (!sKey) { return false; }
    return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
  },
  keys: function () {
    var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
    for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
    return aKeys;
  }
};
