$(document).ready(function()
{	
	addListeners();
});


function addListeners()
{
	$('.ordering').sortable();
	$('.ordering').disableSelection();
	
	$('#save_button').click(function()
	{
		saveOrdering();
	});	
}


function saveOrdering()
{
	var i = 0;
	var items = [];
	$('.ordering li').each(function()
	{
		items.push
		( 
			{
				'item_id': $(this).attr('item_id'),
				'ordering': i
			}
		);
		i++;
	});
	
	$.ajax({
        url: rootUrl + 'backend/save_ordering',
        data: JSON.stringify(items),
        contentType: "application/json; charset=utf-8",
        type:"POST",
        dataType: "json",
        cache: false,
        success: function(data) 
        {
        	alert(data.message);
        }
    });		
}